from paho.mqtt import client as mqtt_client
import random 
import time 
import threading

broker = "test.mosquitto.org"
port = 1883
topic_send = "mqtt/sendThread"
topic_recive = "mqtt/reciveThread"
clientID = f"Test MQTT-{random.randint(0, 10000)}"

class ReadSensorData(threading.Thread):
    def __init__(self, sensorDatas, condition):
        threading.Thread.__init__(self)
        self.sensorDatas = sensorDatas
        self.condition = condition
    def run(self): 
        while True: 
            sensorData1 = random.randint(0, 256)
            sensorData2 = random.randint(0, 256)
            jsonData = f"\"Data1\":\"{sensorData1}\",\"Data2\":\"{sensorData2}\""
            self.condition.acquire() # yeu cau khoa 
            self.sensorDatas.append(jsonData)
            self.condition.notify()
            self.condition.release()
            time.sleep(2)

class PushSensorData(threading.Thread): 
    def __init__(self, client, sensorDatas, conditon): 
        threading.Thread.__init__(self)
        self.sensorDatas = sensorDatas
        self.condition = conditon
        self.client = client
    def run(self): 
        
        while True: 
            self.condition.acquire() # yeu cau khoa
            while True: 
                if self.sensorDatas: 
                    jsonData = self.sensorDatas.pop()
                    print(f"Json data: {jsonData}")
                    publish(self.client, jsonData)
                    break
                self.condition.wait() # wait data push to list
            self.condition.release() # release key 

def connect_mqtt(): 
    def on_connect(clinet, userdata, flag, rc):
        if rc == 0: 
            print("Connected to MQTT Broker")
        else: 
            print("Failed to Connect, return code %d\n", rc)
    
    client = mqtt_client.Client(clientID)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def publish(client, msg): 
    result = client.publish(topic_send, msg)
    status = result[0] 
    if status == 0: 
        print(f"Send {msg} to topic {topic_send} ok")
    else: 
        print(f"Fail to send message to topic {topic_send}")

def subscribe(client: mqtt_client): 
    def on_message(clinet, userdata, msg):
        print(f"Recived {msg.payload.decode()} from {msg.topic} topic")
    client.subscribe(topic_recive)
    client.on_message = on_message

def mqtt_loop(client): 
    subscribe(client)
    client.loop_start()
'''
def mqtt_sub_loop(client): 
    subscribe(client)
    client.loop_forever() 
'''

def run_main(): 
    client = connect_mqtt()
    sensorDataJson = []
    Condition = threading.Condition()

    sensorThread = ReadSensorData(sensorDataJson, Condition)
    pushSensorThread = PushSensorData(client, sensorDataJson, Condition)

    mqttPubThread = threading.Thread(target=mqtt_loop, args=(client,))
    #mqttSubThread = threading.Thread(target=mqtt_sub_loop, args=(client,))

    sensorThread.start()
    pushSensorThread.start()
    #mqttSubThread.start()
    mqttPubThread.start() 

    sensorThread.join()
    pushSensorThread.join()
    mqttPubThread.join()
    #mqttSubThread.join() 

if __name__ == "__main__": 
    run_main() 
