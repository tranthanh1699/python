from ast import While
from concurrent.futures import thread
from re import U
from sys import maxsize
import threading 
import queue 
import time 
import random
from turtle import delay 


class Sensor(threading.Thread): 
    def __init__(self, dataQueue): 
        threading.Thread.__init__(self)
        self.dataQueue = dataQueue
    def run(self): 
        while True: 
            sensorData1 = random.randint(0, 300) 
            sensorData2 = random.randint(0, 300)
            data = f"data1:{sensorData1},data2{sensorData2}"
            self.dataQueue.put(data)
            time.sleep(2) 
class SendSensorData(threading.Thread): 
    def __init__(self, dataQueue): 
        threading.Thread.__init__(self)
        self.dataQueue = dataQueue
    
    def run(self): 
        while True: 
            if not self.dataQueue.empty(): 
                data = self.dataQueue.get() 
                print("Sensor Data: " + data)
            time.sleep(2)
def main(): 
    dataQueue = queue.Queue()
    dataQueue.maxsize = 20

    sensor = Sensor(dataQueue)
    sendSensor = SendSensorData(dataQueue)

    sensor.start()
    sendSensor.start()

    sensor.join()
    sendSensor.join()

if __name__ == "__main__":
    main()