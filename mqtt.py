from http import client
from pydoc import cli
from unittest import result
from paho.mqtt import client as mqtt_client
import random
import time


broker = "test.mosquitto.org"
port = 1883
topic = "mqtt/reciveThread"
client_id = f'python-mqtt-{random.randint(0, 1000)}'

def connect_mqtt(): 
    def on_connect(client, userdata, flags, rc):
        if rc == 0: 
            print("Connected to MQTT Broker")
        else: 
            print("Failed to connect, return code %d\n", rc)
    
    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client
def publish(client):
    msg_count = 0
    while True: 
        time.sleep(1)
        msg = f"message: {msg_count}"
        result = client.publish(topic, msg)
        status = result[0]
        if status == 0:
            print(f"Send {msg} to topic {topic}")
        else: 
            print(f"Fail to send message to topic {topic}")
        msg_count += 1

def subscribe(client: mqtt_client): 
    def on_message(clinet, userdata, msg):
        print(f"Recived {msg.payload.decode()} from {msg.topic} topic")
    
    client.subscribe(topic)
    client.on_message = on_message


def run(): 
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()

if __name__ == "__main__": 
    run()