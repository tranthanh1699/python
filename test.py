import random
import threading
import time
class Producer(threading.Thread): 
    def __init__(self, integers, event): 
        threading.Thread.__init__(self) 
        self.integers = integers 
        self.event = event
    def run(self):
        while True: 
            integer = random.randint(0, 256)
            self.integers.append(integer)
            print("{} appended an integer {}".format(self.name, integer))
            print("event set by {}".format(self.name))
            self.event.set()
            self.event.clear()
            print("event cleared by {}".format(self.name))
            time.sleep(1)

class Comsumer(threading.Thread):
    def __init__(self, integers, event):
        threading.Thread.__init__(self)
        self.integers = integers
        self.event = event
    def run(self):
        while True:
            self.event.wait()
            try:
                integer = self.integers.pop()
                print("{} popped an integer {}".format(self.name, integer))
            except IndexError:
                time.sleep(1)
def main(): 
    integers = [] 
    event = threading.Event()
    producer = Producer(integers, event) 
    consumer = Comsumer(integers, event) 
    producer.start() 
    consumer.start() 
    producer.join() 
    consumer.join()
if __name__ == '__main__': 
    main()