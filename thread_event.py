import threading
import time

class PushEvent(threading.Thread): 
    def __init__(self, event): 
        threading.Thread.__init__(self)
        self.event = event
    def run(self):
        while True: 
            print("Set Event by thread {}".format(self.name))
            self.event.set()
            print("Clear Event by thread {}".format(self.name))
            self.event.clear()
            time.sleep(1) 
    
class WaitEvent(threading.Thread): 
    def __init__(self, event): 
        threading.Thread.__init__(self)
        self.event = event
    def run(self): 
        while True: 
            print("Wait Event by thread {}".format(self.name))
            self.event.wait() 
            print("Get Event by thread {}".format(self.name))  
            time.sleep(0.5)

def run_main(): 
    
    event = threading.Event()
    event2 = threading.Event() 
    pEvent1 = PushEvent(event) 
    wEvent1 = WaitEvent(event) 
    wEvent2 = WaitEvent(event2)

    pEvent1.start()
    wEvent1.start()
    wEvent2.start() 

    pEvent1.join()
    wEvent1.join() 
    wEvent1.join() 
if __name__ == '__main__':
	run_main()


import sqlite3

conn = sqlite3.connect("car.db")
cursor = conn.cursor()

"""
conn.execute('''CREATE TABLE car
         (
            name text,
            brand text, 
            year integer
         );''')  
"""

#print("Table created successfully")
"""
conn.execute("INSERT INTO car (name, brand, year) \
    VALUES('LuxA', 'Vin', 2018)")
conn.execute("INSERT INTO car (name, brand, year) \
    VALUES('LuxSA', 'Vin', 2020)")
conn.execute("INSERT INTO car (name, brand, year) \
    VALUES('CX5', 'Mazda', 2021)")
cursor.execute("INSERT INTO car (name, brand, year) \
    VALUES('CX58', 'Mazda', 2022)")
conn.commit() 
"""
conn.execute("UPDATE car set year = 2032 where name = \"LuxA\" ")
readCursor = conn.execute("SELECT name, brand, year from car")
for row in readCursor: 
    print("NAME: ", row[0])
    print("Brand: ", row[1])
    print("Year: ", row[2])
    print("\n")
conn.close()